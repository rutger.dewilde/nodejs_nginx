

;(function () {

    function htmlToElement(html) {
        var template = document.createElement('template');
        html = html.trim(); // Never return a text node of whitespace as the result
        template.innerHTML = html;
        return template.content.firstChild;
    }

    const refreshTodos = function() {
        fetch('http://localhost:3000/todos')
        .then((response) => {
            return response.json();
        })
        .then((todos) => {
            let todoElements = document.querySelectorAll('.todos-container .todo');
            todoElements.forEach(todoElement => { 
                todoElement.remove();
            });
            todos.forEach(td => { 
                let sp1 = htmlToElement('<div class="todo">' +
                    '    <p>' + td.todo + '</p>' +
                    '    <span>' +
                    '        <a href="#" class="edit-todo" data-id="' + td.id + '"><i class="fas fa-pen"></i></a>' +
                    '        <a href="#" class="del-todo" data-id="' + td.id + '"><i class="fas fa-times fa-lg"></i></a>' +
                    '    </span>\n' +
                    '</div>');
                let sp2 = document.querySelector(".todos-container .add-todo");

                // Get the parent element
                let parentDiv = sp2.parentNode

                // Insert the new element into before sp2
                parentDiv.insertBefore(sp1, sp2)
            });
        })
        .catch((err) => console.log(err));
    };

    const removeTodo = function(id) {
        fetch(encodeURI('http://localhost:3000/todos/' + id), {
            method: "DELETE",
        })
        .then((response) => {
            if (response.status === 200) {
                refreshTodos();
            }
        })
        .catch((err) => console.log(err));
    };

    const addTodo = function(todo) {
        fetch('http://localhost:3000/todos', {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                todo: todo,
            }),
        })
        .then((response) => {
            if (response.status === 201) {
                refreshTodos();
            }
        })
        .catch((err) => console.log(err));
    };

    // @TODO: editTodo


    window.addEventListener('load', (event) => {
        refreshTodos();

        const container = document.querySelector('.todos-container');

        container.addEventListener('click', (event) => {

             // we'll use event bubbling and can check out the target here
            console.log(event.target);

            if (event.target.classList.contains('fa-times')) {
                let deleteLink = event.target.parentNode;
                removeTodo(deleteLink.dataset.id);
            }

            if (event.target.classList.contains('fa-plus-circle')) {
                let modal = document.querySelector('#addTodoModal');
                modal.style.display = 'block';
                modal.focus();
            }

            


        });


        // @TODO
        // add click event listeners to
        //    '#addTodoModal .close'
        //    '#addTodoModal .button-add'
        //    '#addTodoModal .button-cancel'
        //    and so on

    });
})();