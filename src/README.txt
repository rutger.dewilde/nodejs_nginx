Install Node.js and NPM or Yarn
https://www.npmjs.com/get-npm
https://yarnpkg.com/lang/en/docs/install

// install express, body-parser and sqlite3
> npm install
Or
> yarn

// start the API and listen to port 8080
> node app.js